// Copyright (c) 2013 Phillip Bond
// Licensed under the MIT License
// see file LICENSE

package systemstat

import (
	// "fmt"
	"testing"
	"time"
)


var (
	msgFail = "%v method fails. Expects %v, returns %v"
)

func TestNetUsage(t *testing.T) {
	s1 := getNetUsage("testdata/net_dev1")
	time.Sleep(1 * time.Second)
	s2 := getNetUsage("testdata/net_dev2")
	time.Sleep(1 * time.Second)
	s3 := getNetUsage("testdata/net_dev3")
	time.Sleep(1 * time.Second)
	s4 := getNetUsage("testdata/net_dev4")
	time.Sleep(1 * time.Second)
	s5 := getNetUsage("testdata/net_dev5")

	info := getNetSpeed(s1, s2)
	devStats := info.Interfaces["venet0"]
	t.Logf("Name: %v, Rbps: %v, RPks: %v, Tbps: %v, TPks: %v\n", devStats.Name, devStats.RBps, devStats.RPks, devStats.TBps, devStats.TPks)
	info = getNetSpeed(s2, s3)
	devStats = info.Interfaces["venet0"]
	t.Logf("Name: %v, Rbps: %v, RPks: %v, Tbps: %v, TPks: %v\n", devStats.Name, devStats.RBps, devStats.RPks, devStats.TBps, devStats.TPks)
	info = getNetSpeed(s3, s4)
	devStats = info.Interfaces["venet0"]
	t.Logf("Name: %v, Rbps: %v, RPks: %v, Tbps: %v, TPks: %v\n", devStats.Name, devStats.RBps, devStats.RPks, devStats.TBps, devStats.TPks)
	info = getNetSpeed(s4, s5)
	devStats = info.Interfaces["venet0"]
	t.Logf("Name: %v, Rbps: %v, RPks: %v, Tbps: %v, TPks: %v\n", devStats.Name, devStats.RBps, devStats.RPks, devStats.TBps, devStats.TPks)
}

func TestGetUptime(t *testing.T) {
	s := getUptime("testdata/uptime")
	if s.Uptime != 18667.53 {
		t.Errorf(msgFail, "getUptime", "18667.53", s.Uptime)
	}
}

func TestGetLoadAvgSample(t *testing.T) {
	s := getLoadAvgSample("testdata/loadavg")
	t.Logf("%#v\n", s)
	if s.One != 0.1 {
		t.Errorf(msgFail, "getUptime", "0.1", s.One)
	}
	if s.Five != 0.15 {
		t.Errorf(msgFail, "getUptime", "0.15", s.Five)
	}
	if s.Fifteen != 0.14 {
		t.Errorf(msgFail, "getUptime", "0.14", s.Fifteen)
	}
}
